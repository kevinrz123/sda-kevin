import java.io.*;

public class Searching {
        long X[];
        int N;
        Searching() {
            try {
                BufferedReader br = new BufferedReader (new InputStreamReader(System.in));
		        N = Integer.parseInt(br.readLine());
                X = new long[N+1];
                X[0] = 0;
		for (int i = 1; i <= N; i++) 
		    X[i] =  X[i-1]+Integer.parseInt(br.readLine());
		int M = Integer.parseInt(br.readLine());
		for(int i = 0; i < M; i++) {
			String v[] = br.readLine().split(" ");
			cari(Integer.parseInt(v[0]), Integer.parseInt(v[1]));
                }
            }
            catch (Exception e) { System.out.println(e); }
	}
	public static void main(String[] args) {
               new Searching();
        }
             
        //-----------------------------------------------
        // isi method ini dengan algoritma pencarian yang sesuai
        // anda boleh menambahkan method tambahan untuk membantunya
        // tetapi tidak boleh mengubah satu barispun bagian template
        // di luar ini.

        void cari(int t, long V) {

            long hasiltv = V + X[t];
            int k = N / 2;
            int bawah = 0;
            int atas = N;
            int interval = k;
            boolean tidakAda = false;

            while (!((X[k-1] < hasiltv) && (hasiltv <= X[k]))) {

                // Kalo interval tinggal 1
                if (interval <= 1) {
                    k = bawah;
                    if (!((X[k-1] < hasiltv) && (hasiltv <= X[k]))) {
                        k = atas;
                        if (hasiltv > X[N]) {
                            tidakAda = true;
                            break;
                        }
                    }
                }

                // Kalo kiri kebanyakan
                else if (!(X[k-1] < hasiltv)) {
                    atas = k - 1;
                    interval = atas - bawah;
                    k = k - (interval / 2);

                }

                // Kalo kanan kebanyakan
                else if (!(hasiltv <= X[k])) {
                    bawah = k + 1;
                    interval = atas - bawah;
                    k = k + (interval / 2);
                }
            }

            if (tidakAda) {
                System.out.println("TIDAK ADA");
            } else {
                System.out.println(k);
            }

        }

        //---------------------------------------------------------------
}